<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= htmlspecialchars($title); ?></title>
        
        <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/usr/css/response.css" />
        <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/epiqworx/css/style.css" />
        <?php if($action !=='dberror'){?>
        <link rel="stylesheet" type="text/css"   href="<?= PATH; ?>/main/usr/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?= PATH; ?>/main/usr/etc/bootstrap/css/bootstrap.min.css" />
            <?php }?>
    </head>
    <body>
        <section class="page-wrap">
            <header class="main">
                <h1>NAIL IT</h1>
                <nav class="<?=$nav;?>">
                    <ul class="nav nav-pills">
                        <li><a href="<?= PATH;?>">Home</a></li>
                        <li><a href="?action=about">About Us</a></li>
                        <li><a href="?action=contact-us">Contact Us</a></li>
                        <li><a href="?action=register">Register</a></li>
                        <li><a href="?action=apponitment">Appointment</a></li>
                        <li><a href="?action=services">Services</a></li>
                        <li><a href="?action=products">Products</a></li>
                        <li><a href="?action=gallery">Gallery</a></li>
                    </ul>
                </nav>
            </header>