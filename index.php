<?php
require_once 'main/epiqworx/epiqrithm/full.php';    //for algoritms
require_once 'main/epiqworx/db/handler.php';    // database PDO
require_once 'main/model.php';
$connected = dbAccess::test('nailit'); //  ------------------------------------ test database connection
$nav = $flash = null;   //------------------------------------------------------------- Color Fill Special Effect on Page Load (inactive)

/*
 * start controller
 */
$action = filter_input(INPUT_POST, 'action');
if ($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL) {
        $action = 'home';//return to the home page
    }
}
switch ($action)
{
case 'home':
    $title = 'Home';
    require_once 'main/view/default/home.php';
    break;
case 'about':
    $title = 'About Us';
    require_once 'main/view/pages/about-us.php';
    break;
case 'register':
    $title = 'Register';
    require_once 'main/view/pages/register.php';
    break;
case 'apponitment':
    $title = 'Appointment';
    require_once 'main/view/pages/appointment.php';
    break;
case 'gallery':
    $title = 'Gallery';
    require_once 'main/view/pages/gallery.php';
    break;
case 'products':
    $title = 'Products';
    require_once 'main/view/pages/products.php';
    break;
case 'services':
    $title = 'Services';
    require_once 'main/view/pages/services.php';
    break;
case 'contact-us':
    $title = 'Services';
    require_once 'main/view/pages/services.php';
    break;
default :
    echo "case not handled for action <b>$action</b>";
    break;
}